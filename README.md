# Programming Foundation Fundamentals
With Annyce Davis
<table>
<tr>
<td>Skill level: Beginner</td>
<td>Duration: 2h 10m</td>
<td>Released: 7/22/2019</td>
</tr>
</table>

*Click [here](https://www.linkedin.com/learning/programming-foundations-fundamentals-3) for course link*

* [ ] Earn a sharable certificate for my LinkedIn account: [Rafael Perea](https://www.linkedin.com/in/rafaelperea/)

## Course details

```
Gain the core knowledge to begin programming in any language. In this course, instructor Annyce Davis uses Python to explore the core concepts and structure of programming languages and helps you grasp what’s going on under the hood. After going over basic statements and expressions in Python, Annyce explores strings, variables, and conditional code—which are common topics in all programming languages. She also shows how to work with different kinds of data and troubleshoot a variety of errors. Along the way, she compares how code is written in several different languages and provides guidance on the criteria to use when choosing a programming language. Upon wrapping up this course, you’ll have the knowledge you need to continue your coding journey in whichever language piques your interest.
```

## Mixing in other languages
I'll use my preferred language (ruby),

[Basic Git Checkout **rbenv**](https://github.com/rbenv/rbenv#basic-git-checkout)
[Clone **ruby-build** as rbenv plugin using git](https://github.com/rbenv/ruby-build#clone-as-rbenv-plugin-using-git)

```bash
alias update-rbenv-and-ruby-build-plugin='echo "" && cd "$(rbenv root)" && echo $(rbenv -v) && git pull --no-rebase --ff && echo "" && echo "ruby-build:" && git -C "$(rbenv root)"/plugins/ruby-build pull && echo "" && cd .'`
```
For my convenience, I append above to the `/.bash_aliases` file, or, simply pull both git repositories, if in Windows or Mac.

```bash
source /.bashrc
```
```
update-rbenv-and-ruby-build-plugin
```

version as of this writing.
- Ruby-3.2.1 

## Tutorial Status
* [X] *Porfolio: setting up git repository for this course* [2023-02-10]
- [x] *Introduction* [2023-02-08]
  - [X] *The fundamentals of programming (Viewed) 55s*
  - [X] *Following along with the course (Viewed) 1m 37s*
- [X] *Programming Basics* [2023-02-08]
  - [X] *2m 52s What is programming?* [2023-02-08]
  - [X] *3m 36s What is a programming language?* [2023-02-08]
  - [X] *3m 16s Writing source code* [2023-02-08]
  - [X] *2m 41s Running your code* [2023-02-10]
  - [X] *3m 21s Using an IDE*: **code** [2023-02-10]
  - [X] *14/14 questions Chapter Quiz* with one corrected wrong answer [2023-02-10]
    - [X] *Interpreter needs to run: example `python3` interprets the `01_03.py` file, as `ruby` is to `01_03.py`.* [2023-02-10]
- [X] *Programming Syntax* [2023-02-10]
  - [X] *1m 52s Why Python?* [2023-02-10]
  - [X] *4m 19s Installing Python on a Mac* [2023-02-10]
  - [X] *1m 49s Installing Python on Windows* ***WSL*** [2023-02-10]
  - [X] *3m 8s Running Python on a command line on Mac* [2023-02-10]
  - [X] *4m 10s Running Python on a command line on Windows* [2023-02-10]
  - [X] *3m 43s Installing Visual Studio on Mac* [2023-02-10]
  - [X] *3m 40s Installing Visual Studio on Windows* [2023-02-10]
  - [X] *3m 37s Running Python in an IDE* [2023-02-10]
  - [X] *4m 2s Basic statements and expressions* [2023-02-11]
  - [X] *4m 2s Troubleshoot issues* [2023-02-11]
  - [X] *20/20 questions Chapter Quiz* [2023-02-11]
- [X] *Variable and Data Types* [2023-02-12]
  - [X] *3m 37s Introduction to variables and data types* [2023-02-12]
  - [X] *3m 27s Variable across languages* [2023-02-12]
  - [X] *4m 48s Working with numbers* [2023-02-12]
  - [X] *2m 29s Working with strings* [2023-02-12]
  - [X] *4m 22s Properly using whitespace* [2023-02-12]
  - [X] *3m 26s Working with comments* [2023-02-12]
  - [X] *49s Challenge: What's the output?* [2023-02-12]
  - [X] *5m 14s Solution: What's the output?* [2023-02-12]
  - [X] *17/17 questions Chapter Quiz*with one corrected wrong answer [2023-02-12]
    - [X] *Empty lines (el) are ignored unless inside special characters (el ends a code block)
- [X] *Conditional Code* [2023-02-12]
  - [X] *5m 37s Making decisions in code* [2023-02-12]
  - [X] *4m 49s Exploring conditional code* [2023-02-12]
  - [X] *4m 15s Working with simple conditions* [2023-02-12]
  - [X] *1m 52s Conditionals across languages* [2023-02-12]
  - [X] *1m 11s Challenge: Guessing game* [2023-02-12]
  - [X] *1m 58s Solution: Guessing game* [2023-02-12]
  - [X] *12 questions Chapter Quiz* [2023-02-12]
- [ ] Modular Code
  - [ ] 3m 45s Introduction to functions
  - [ ] 3m 47s Creating and calling functions
  - [ ] 5m 29s Setting parameters and arguments
  - [ ] Returning values from functions
  - [ ] 3m 26s Functions across languages
  - [ ] 1m Challenge: Favorite cities
  - [ ] 1m 48s Solution: Favorite cities
  - [ ] 15 questions Chapter Quiz
- [ ] Conclusion
  - [ ] Exploring languages
  - [ ] Next steps



<!-- 

## Getting started

To make it easy for you to get started with GitLab, here's a list of recommended next steps.

Already a pro? Just edit this README.md and make it your own. Want to make it easy? [Use the template at the bottom](#editing-this-readme)!

## Add your files

- [ ] [Create](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/rkcperea/programming-foundation-fundamentals.git
git branch -M main
git push -uf origin main
```

## Integrate with your tools

- [ ] [Set up project integrations](https://gitlab.com/rkcperea/programming-foundation-fundamentals/-/settings/integrations)

## Collaborate with your team

- [ ] [Invite team members and collaborators](https://docs.gitlab.com/ee/user/project/members/)
- [ ] [Create a new merge request](https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html)
- [ ] [Automatically close issues from merge requests](https://docs.gitlab.com/ee/user/project/issues/managing_issues.html#closing-issues-automatically)
- [ ] [Enable merge request approvals](https://docs.gitlab.com/ee/user/project/merge_requests/approvals/)
- [ ] [Automatically merge when pipeline succeeds](https://docs.gitlab.com/ee/user/project/merge_requests/merge_when_pipeline_succeeds.html)

## Test and Deploy

Use the built-in continuous integration in GitLab.

- [ ] [Get started with GitLab CI/CD](https://docs.gitlab.com/ee/ci/quick_start/index.html)
- [ ] [Analyze your code for known vulnerabilities with Static Application Security Testing(SAST)](https://docs.gitlab.com/ee/user/application_security/sast/)
- [ ] [Deploy to Kubernetes, Amazon EC2, or Amazon ECS using Auto Deploy](https://docs.gitlab.com/ee/topics/autodevops/requirements.html)
- [ ] [Use pull-based deployments for improved Kubernetes management](https://docs.gitlab.com/ee/user/clusters/agent/)
- [ ] [Set up protected environments](https://docs.gitlab.com/ee/ci/environments/protected_environments.html)

***

# Editing this README

When you're ready to make this README your own, just edit this file and use the handy template below (or feel free to structure it however you want - this is just a starting point!). Thank you to [makeareadme.com](https://www.makeareadme.com/) for this template.

## Suggestions for a good README
Every project is different, so consider which of these sections apply to yours. The sections used in the template are suggestions for most open source projects. Also keep in mind that while a README can be too long and detailed, too long is better than too short. If you think your README is too long, consider utilizing another form of documentation rather than cutting out information.

## Name
Choose a self-explaining name for your project.

## Description
Let people know what your project can do specifically. Provide context and add a link to any reference visitors might be unfamiliar with. A list of Features or a Background subsection can also be added here. If there are alternatives to your project, this is a good place to list differentiating factors.

## Badges
On some READMEs, you may see small images that convey metadata, such as whether or not all the tests are passing for the project. You can use Shields to add some to your README. Many services also have instructions for adding a badge.

## Visuals
Depending on what you are making, it can be a good idea to include screenshots or even a video (you'll frequently see GIFs rather than actual videos). Tools like ttygif can help, but check out Asciinema for a more sophisticated method.

## Installation
Within a particular ecosystem, there may be a common way of installing things, such as using Yarn, NuGet, or Homebrew. However, consider the possibility that whoever is reading your README is a novice and would like more guidance. Listing specific steps helps remove ambiguity and gets people to using your project as quickly as possible. If it only runs in a specific context like a particular programming language version or operating system or has dependencies that have to be installed manually, also add a Requirements subsection.

## Usage
Use examples liberally, and show the expected output if you can. It's helpful to have inline the smallest example of usage that you can demonstrate, while providing links to more sophisticated examples if they are too long to reasonably include in the README.

## Support
Tell people where they can go to for help. It can be any combination of an issue tracker, a chat room, an email address, etc.

## Roadmap
If you have ideas for releases in the future, it is a good idea to list them in the README.

## Contributing
State if you are open to contributions and what your requirements are for accepting them.

For people who want to make changes to your project, it's helpful to have some documentation on how to get started. Perhaps there is a script that they should run or some environment variables that they need to set. Make these steps explicit. These instructions could also be useful to your future self.

You can also document commands to lint the code or run tests. These steps help to ensure high code quality and reduce the likelihood that the changes inadvertently break something. Having instructions for running tests is especially helpful if it requires external setup, such as starting a Selenium server for testing in a browser.

## Authors and acknowledgment
Show your appreciation to those who have contributed to the project.

## License
For open source projects, say how it is licensed.

## Project status
If you have run out of energy or time for your project, put a note at the top of the README saying that development has slowed down or stopped completely. Someone may choose to fork your project or volunteer to step in as a maintainer or owner, allowing your project to keep going. You can also make an explicit request for maintainers. -->
