# syntax error
print("Hello world"
# > cannot be read by interpreter

# runtime error
10 * (2/0)
# > correct syntax but cannot be run.

# semantic error
name = "Alice"
print("Hello name")
# > silent errors