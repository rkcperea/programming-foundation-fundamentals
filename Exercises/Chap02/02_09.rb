puts "Input first value a = "
a = gets.chomp.to_i
puts "Input second value, b = "
b = gets.chomp.to_i

puts "__________________________" 
puts "| c |          sum        |" 
puts "| d |      difference     |" 
puts "| e |        product      |" 
puts "| f |       quotient      |" 
puts "| g |        power        |" 
puts "| h |  whole number part  |" 
puts "| i | mixed no. numerator |" 
puts "| a |  h*b + i  numerator |" 
puts "| b |     denominator     |" 
puts '"""""""""""""""""""""""""""' 

puts "c = a + b = #{a} + #{b} = #{ a + b }"
puts "d = a - b = #{a} - #{b}  = #{ a - b }"
puts "e = a * b = #{a} * #{b}  = #{ a * b }"
puts "f = a.to_f / b = #{a.to_f} / #{b}  = #{ a.to_f / b }"
puts "g = a ** b = #{a} ** #{b} = #{ a ** b }"
h, i = [a / b, a % b]
puts "h = a / b = #{a} / #{b} = #{ h }"
puts "i = a % b = #{a} % #{b}  = #{ i }"
puts "a = h * b + i = #{h} * #{b} + #{i} = #{ h * b + i }"
puts "#{a}/#{b} = #{h} and #{i}/#{b}" 