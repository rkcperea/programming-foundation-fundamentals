puts "Hi, what's your name? "
name = gets.chomp
puts "How old are you? "
age = gets.chomp.to_i

if (age < 13)
    puts("You're too young to register", name)
else
    puts("Feel free to join, #{age}-year-old #{name}!")
end