print("Challenge 1:")

# A message for the user
message = "This is going to be tricky ;)"
Message = "Very tricky!"
print(message) # show the message on the screen

# Perform mathematical operations
result = 2**3
print("2**3 =", result)
result = 5 - 3
#print("5 - 3 =", result)

print("Challenge complete!")

print("Practice Exercise 2:")
message = "Hope you're having fun!"
print("The message", message)
# Perform mathematical operations
answer = 5+2**3
print("the answer is:", answer)
result = 5 - 3
#print("5 - 3 =", result)
print("Done!")


# OUTPUT is after the empty line

# Challenge 1: 
# This is going to be tricky ;)
# 2**3 = 8
# Challenge complete!
# Practice Exercise 2:
# The message Hope you're having fun!
# the answer is: 13
# Done!