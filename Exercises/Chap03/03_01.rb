age = 36
puts age
puts age.class

height_in_cm = 167.64
puts height_in_cm
puts height_in_cm.class

big_num = 167.64e-123
puts big_num
puts big_num.class

complex_num = 167+64i
puts complex_num
puts complex_num.class

email_address = "john.doe@me.com"
puts email_address
puts email_address.class

bool = true
puts bool
puts bool.class

bool = false
puts bool
puts bool.class

bool = nil
puts bool
puts bool.class
puts "negating: !#{bool.inspect}"
puts (!bool).class
