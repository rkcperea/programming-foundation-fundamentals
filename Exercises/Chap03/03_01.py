age = 36
print(age)
print(type(age))

height_in_cm = 167.64
print(height_in_cm)
print(type(height_in_cm))

big_num = 167.64e-123
print(big_num)
print(type(big_num))

complex_num = 167+64j
print(complex_num)
print(type(complex_num))

email_address = "john.doe@me.com"
print(email_address)
print(type(email_address))

true = False
print(true)
print(type(true))
